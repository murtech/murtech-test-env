<?php 
			 
	#Authorization using uWaterloo CAS
	#https://strobe.uwaterloo.ca/~twiki/bin/view/ISTCSS/CASClientAuth#PHP
	require_once('/usr/share/pear/CAS.php');
	phpCAS::client(CAS_VERSION_2_0, 'cas.uwaterloo.ca' , 443, '/cas');
	phpCAS::setCasServerCACert('/etc/pki/tls/certs/globalsignchain.crt');
	
	#Determine application root location
	$app_root = '';
	if ($_SERVER['HTTP_HOST'] == 'mur.uwaterloo.ca')
		$app_root = '/var/www/html/mur';
	elseif ($_SERVER['HTTP_HOST'] == 'pole.uwaterloo.ca')
		$app_root = '/var/www/html/urp/urpdev';

	#Authorization Snippet - DO NOT DELETE!
	//session_start();
	//$_SESSION['DOCUMENT_ROOT'] = getcwd();
	//$app_root = '/fsys3/www/html/urp/urpdev/';
	//$_SESSION['SERVER_HOST'] = $_SERVER['REQUEST_URI'];
	setcookie('SESSID', session_id(), 0, '/');

	$myServer = "localhost-drupal7";
	$myUser = "murtech";
	$myPass = "6yhn&UJM8ik,";
	$myDB = "drupal";
	
	$link = new mysqli($myServer, $myUser, $myPass, $myDB);
	
	if(!$link)
	{
		echo '<p><font color="red">The site is currently experiencing technical difficulties. We apologize for any inconvenience.</font></p>';
		exit();
	}
	if(!mysqli_select_db($link, "urp"))
	{
		echo '<p><font color="red">The site is currently experiencing technical difficulties. We apologize for any inconvenience.</font></p>';
		exit();
	}
	if (isset($_GET['logout']))
		phpCAS::logout();


		
	function authenticateUser($dbaccess, $username) {
		$userid = '';
		$checkAuthenticityQuery = mysqli_query($dbaccess, "SELECT user_id FROM mur_users WHERE username = '$username'");
	
		// Formulate the SQL query find the user
		if($checkAuthenticityQuery)
		{
			if(!mysqli_num_rows($checkAuthenticityQuery))
			{
				return false;
			}
			else
			{
				$row = mysqli_fetch_array($checkAuthenticityQuery);
				$userid = $row['user_id'];
				return $userid;
			}
		}
		else
		{
			echo "failed to even do the query";
			return false;
		}
	}

	// $type is 0 when a user tries to access a page he/she doesn't have permission to see
	// 1 when a user doesn't exist in the system
	function authFailed($type) {  	
		if ($type == 0)
		{
			?>
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
						<title>MUR Login Unsuccessful</title>
						<link rel="stylesheet" href="css/style.css" type="text/css" />
					</head>
					
					<body>
					<h1>Unauthorized Access!</h1>
						<div class="center"> You are not authorized to view this page!</div>
						<br />
						<div class="center"> This can happen if you try to view a page which your user account does not have permissions for. </div>
						<div class="center"> Please double check the URL or ensure you're using a MUR account which can access this page. <a href="?logout=true">Log out of CAS </a>and try again.</div>
			
					</body>
				</html>
				<? 
				exit;
		} 
		else
		{
			?>
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
						<title>MUR Login Unsuccessful</title>
						<link rel="stylesheet" href="css/style.css" type="text/css" />
					</head>
					
					<body>
					<h1>Mur Login Unsuccessful!</h1>
						<div class="center"> You have not logged in successfully!</div>
						<br />
						<div class="center"> This can happen if you logged in to CAS successfully, but your user account does not exist on the MUR system.</div>
						<div class="center"> Please ensure you're using the proper MUR account. <a href="?logout=true">Log out of CAS </a>and try again.</div>
			
					</body>
				</html>
				<? 
				exit;	
		}
	}
	
	function getPermissions($dbaccess, $userid)
	{
		$getPermissions = mysqli_query($dbaccess, "SELECT group_id FROM mur_users JOIN mur_permission_listing USING (user_id) WHERE user_id = '$userid'");
	
		$i = 0;
		while($row = mysqli_fetch_assoc($getPermissions)){
			$groupid[$i] = $row['group_id'];
			$i++;
		}
		return $groupid;
	}

	function verifyPermissions($groupid)
	{
		// if the user does not have the $groupid permission and is not admin
		if (!(in_array($groupid,$_SESSION['groupid']) || in_array(1, $_SESSION['groupid'])))
		{
			authFailed(0);
		}
		
	}
	
	function updateLastLogin($dbaccess, $userid)
	{
		if ($query = mysqli_prepare($dbaccess, "UPDATE mur_users SET last_login = now() WHERE user_id = ?"))
		{
			mysqli_stmt_bind_param($query, "i", $userid); 
			mysqli_stmt_execute($query);
		}
	}

	function login()
	{
		phpCAS::forceAuthentication();
		$username = phpCAS::getUser();
		
		global $link;
		// $authenticated returns the user id of the user
		$authenticated = authenticateUser($link, $username);
		
		if($authenticated)
		{
			$_SESSION["authenticatedUserName"] = $username;
			$_SESSION['authenticatedUserId'] = $authenticated;
			$_SESSION['groupid'] = getPermissions($link, $authenticated);
			updateLastLogin($link, $authenticated);
		}
		else
		{
			authFailed(1);
		}
	}
	
	
	// Check for login status, if not logged in, call login function
	if(!(phpCAS::isAuthenticated() && isset($_SESSION['authenticatedUserName']) && isset($_SESSION['authenticatedUserId'])))
	{
		login();
	}


?>