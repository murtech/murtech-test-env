<?
require_once "../../admin/login.php";
global $link;

if ((isset($_POST['length']) && isset($_POST['time']) && $_POST['length'] != "") || (isset($_POST['never'])))
{
	if ($_POST['activity'] == "inactive")
		$stmt = $link->prepare("SELECT user_id, username, last_login FROM mur_users WHERE last_login NOT BETWEEN ? AND NOW() AND last_login != \"0000-00-00 00:00:00\"");
	else
		$stmt = $link->prepare("SELECT user_id, username, last_login FROM mur_users WHERE last_login BETWEEN ? AND NOW()");
	if (isset($_POST['never']))
		$stmt = $link->prepare("SELECT user_id, username, last_login FROM mur_users WHERE last_login = \"0000-00-00 00:00:00\"");

	if ($stmt)
	{ 
		if (!(isset($_POST['never'])))
			$stmt->bind_param("s", retrieveDate($link));
		$stmt->execute();
		$stmt->store_result();
	
		$numRows = $stmt->num_rows;
	
		if ($numRows!= 0)
		{
			echo '<table border = 1>';
			echo '<tr>
				<th>User ID</th>
				<th>Username</th>
				<th>Last Login</th>
				<th>Delete</th>
				</tr>';
			$stmt->bind_result($userid, $username, $last_login);
			while ($stmt->fetch())
			{
				echo '<tr>
				<td>'.$userid.'</td>
				<td>'.$username.'</td>
				<td>'.$last_login.'</td>
				<td><a href="?delete=true&id='.$userid.'">[X]</a></td>
				</tr>';
			}
			echo '</table>';
		}
		else
		{
			echo 'No users found!';
		}
	} 
}
else
{
	echo 'Please fill in all fields!';	
}

function retrieveDate($dbaccess)
{
	$length = $_POST['length'];
	$time = $_POST['time'];
	$curDate = date('Y-m-d H:i:s');  
	
	$newDate = date('Y-m-d H:i:s', strtotime($curDate . " -".$length. " ".$time));
	return $newDate;
}

?>