<?
require_once "../../admin/login.php";
global $link;

	$UserId = mysqli_real_escape_string($link, $_POST['username']);
	$FirstName = mysqli_real_escape_string($link, $_POST['fName']);
	$LastName = mysqli_real_escape_string($link, $_POST['lName']);
	$Email = mysqli_real_escape_string($link, $_POST['email']);
	$Username = mysqli_real_escape_string($link, $_POST['username']);
	
	if($_GET['delete'])
	{
		// delete a record
		if($deleteUserQuery = mysqli_prepare($link, "DELETE FROM mur_users WHERE user_id = ?"))
		{
			mysqli_stmt_bind_param($deleteUserQuery, 'i', $UserId);
			mysqli_stmt_execute($deleteUserQuery);
			mysqli_stmt_close($deleteUserQuery);
			echo "<p>User deleted successfully!!</p>";
		}
	}
	
    	if($insertUserQuery = mysqli_prepare($link, "INSERT INTO mur_users (username,first_name,last_name,email,registration_date) VALUES (?, ?, ?, ?, NOW())"))
		{
			mysqli_stmt_bind_param($insertUserQuery, 'ssss', $UserId, $FirstName, $LastName, $Email);
			mysqli_stmt_execute($insertUserQuery);
			mysqli_stmt_close($insertUserQuery);
			echo "<p>User added successfully!</p>";
		}
  	

?>