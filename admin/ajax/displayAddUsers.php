<?
require_once "../../admin/login.php";

?>

<h2> Add a User<img id="loading" style="float:right; display:inline" src="images/loading_small.gif"></img></h2>
<div id="results"></div>
<form id="form1" name="form1" method="post" action="javascript:void(0)">
    <table width="948" border="1">
        <tr>
            <td>Username: </td>
            <td><input class="inputField" type="text" name="username" id="username" /></td>
        </tr>
        <tr>
            <td>First name: </td>
            <td><input class="inputField" type="text" name="fName" id="fName" /></td>
        </tr>	
        <tr>
            <td>Last name: </td>
            <td><input class="inputField" type="text" name="lName" id="lName" /></td>
        </tr>	
        <tr>
            <td>Email: </td>
            <td><input class="inputField" type="text" name="email" id="email" /></td>
        </tr>	
    </table>
    <input type="submit" onclick="addUser();" name="ajaxSubmit"  value="Submit"  />
</form>
<h2> Current Users </h2>
       
<table width="948" border="1">
    <tr>
        <th>Username</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Delete</th>        
	</tr> 
                       
<?     
	if($userQuery = mysqli_prepare($link, "SELECT user_id, username, first_name, last_name FROM mur_users ORDER BY username ASC"))
        {
            mysqli_stmt_execute($userQuery);
            mysqli_stmt_bind_result($userQuery, $UserId, $Username, $FirstName, $LastName);
            while(mysqli_stmt_fetch($userQuery))
            {
                echo "<tr>";
                    echo "<td>" . $Username . "</td>";
                    echo "<td>" . $FirstName . "</td>";
					echo "<td>" . $LastName . "</td>";
					echo '<td> <a href="index.php?page=addusers&delete=true&id='.$UserId.'">[X]</a>'; 
                echo "</tr>";
            }
            mysqli_stmt_close($userQuery);
        }
				
?>	
</table>

