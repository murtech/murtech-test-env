<?
require_once "../../admin/login.php";

?>

<h2> Add a Section <img id="loading" style="float:right; display:inline" src="images/loading_small.gif"></img></h2>
<div id="results"></div>
<form id="form1" name="form1" method="post" action="javascript:void(0)">
    <table width="948" border="1">
        <tr>
            <td>Section Name: </td>
            <td><input class="inputField" type="text" name="groupName" id="groupName" /></td>
        </tr>
        <tr>
            <td>Description: </td>
            <td><input class="inputField" type="text" name="groupDesc" id="groupDesc" /></td>
        </tr>	
        <tr>
        	<td>Tab</td>
            <td><select name="groupTab" id="groupTab">
            	<option value="Systems">Systems</option>
                <option value="Reports">Reports</option>
            </select></td>
        </tr>
    </table>
    <input type="submit" onclick="addGroup();" name="ajaxSubmit"  value="Submit"  />
</form>
<h2> Current Sections </h2>
       
<table width="948" border="1">
    <tr>
        <th>Section Name</th>
        <th>Description</th>
	</tr> 
                       
<?     
	if($userQuery = mysqli_prepare($link, "SELECT group_name, group_description FROM mur_groups ORDER BY group_name ASC"))
        {
            mysqli_stmt_execute($userQuery);
            mysqli_stmt_bind_result($userQuery, $groupName, $groupDesc);
            while(mysqli_stmt_fetch($userQuery))
            {
                echo "<tr>";
                    echo "<td>" . $groupName . "</td>";
                    echo "<td>" . $groupDesc . "</td>";
                echo "</tr>";
            }
            mysqli_stmt_close($userQuery);
        }
				
?>	
</table>

