<?
require_once "../../admin/login.php";
global $link;

	$groupName = mysqli_real_escape_string($link, $_POST['groupName']);
	$groupDesc = mysqli_real_escape_string($link, $_POST['groupDesc']);
	$groupTab = mysqli_real_escape_string($link, $_POST['groupTab']);
	
	if($_GET['delete'])
	{
		// delete a record
		if($deleteUserQuery = mysqli_prepare($link, "DELETE FROM mur_users WHERE user_id = ?"))
		{
			mysqli_stmt_bind_param($deleteUserQuery, 'i', $UserId);
			mysqli_stmt_execute($deleteUserQuery);
			mysqli_stmt_close($deleteUserQuery);
			echo "<p>Group deleted successfully!!</p>";
		}
	}
	    //$sql = "INSERT INTO mur_groups (group_name,group_description) VALUES ('$gn','$gd')";

    	if($insertGroupQuery = mysqli_prepare($link, "INSERT INTO mur_groups (group_name,group_description,tab) VALUES (?, ?, ?)"))
		{
			mysqli_stmt_bind_param($insertGroupQuery, 'sss', $groupName, $groupDesc, $groupTab);
			mysqli_stmt_execute($insertGroupQuery);
			mysqli_stmt_close($insertGroupQuery);
			echo "<p>Group added successfully!</p>";
		}
  	

?>