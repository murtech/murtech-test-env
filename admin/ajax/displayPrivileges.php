<?
require_once "../../admin/login.php";

?>

<h2> Modify Privileges <img id="loading" style="float:right; display:inline" src="images/loading_small.gif"></img></h2>
<div id="results"></div>
<form id="form1" name="form1" method="post" action="javascript:void(0)">
    <table width="948" border="1">
        <tr>
            <td>Add user: </td>
			<td>
               
    <select name="user_id">
		<option>Username</option>
		<?php
    
        $query = "SELECT * FROM mur_users ORDER BY username ASC";
        $result = @mysqli_query ($link, $query);
        while ($row = mysqli_fetch_assoc ($result))
		{
            echo "<option value='" . $row['user_id'] . "'";
            if ($row['user_id'] == $user_id)
			{
                echo " selected='selected'";
            }
           echo "> " . $row['username'] . "</option>\n";
        }
        ?>
    </select>
            
            </td>
        </tr>
        <tr>
            <td>To group: </td>
            <td>
                <select name="group_id">
    	<option value="">Group</option>
    
		<?php
        
        $query = "SELECT * FROM mur_groups ORDER BY group_name ASC";
        $result = @mysqli_query ($link, $query);
        $pulldown = '';
        while ($row = mysqli_fetch_array ($result)) {
            $pulldown .= "<option value=\"{$row['group_id']}\"";
            if ($row['group_id'] == $group_id) {
                $pulldown .= " selected='selected'";
            }
            $pulldown .= ">" . $row['group_name'] . "</option>\n";
        }
        echo $pulldown;
        
        ?>
    </select>	
            
            
            </td>
        </tr>	
    </table>
    <input type="submit"  name="ajaxSubmit"  value="Submit" onclick="modifyPrivileges();"  />
</form>
<h2> Current Groups </h2>
       
<table width="948" border="1">
    <tr>
        <th>Group</th>
        <th>Name</th>
        <th>Userid</th>
        <th>Delete</th>
	</tr> 
                       
		<?php
			$privilegeQuery = @mysqli_query($link, "SELECT a.user_id, a.uid, b.first_name, b.last_name, b.username, c.group_name, c.group_description FROM mur_permission_listing a INNER JOIN mur_users b ON a.user_id = b.user_id INNER JOIN mur_groups c ON a.group_id = c.group_id ORDER BY c.group_name, b.username ASC");
			$last_group_name='';
			
			while ($myrow = mysqli_fetch_array($privilegeQuery))
			{

				echo '<tr>';
				if (!($last_group_name==$myrow["group_name"]))
				{
					echo '<td><b>' . $myrow["group_name"] . '</b><br />' . $myrow["group_description"] . '</td>';
				}
				else
				{
					echo '<td>&nbsp;</td>';
				}
				echo '<td alt="' . $myrow["group_description"] . '">' . $myrow['first_name'] . ' ' . $myrow['last_name'] . '</td><td>' . $myrow['username'] . '</td><td>' . '[<a href="index.php?page=privileges&delete=true&uid=' . $myrow['uid'] . '">x</a>]</td></tr>';
				$last_group_name=$myrow["group_name"];
			}
			echo "</table>";
		?>
    </table>


