<?php
include("../mysql/urcpadev.php");
include("../admin/login.php");

$query = "";
if ( (isset($_POST['submit_section'])) || (isset($_POST['submit_page'])) ) {
	if (isset($_POST['submit_section'])) {
		$name = $_POST['section_name'];
		$desc = $_POST['section_desc'];
		$tab = $_POST['add_to_tab'];
		$query = "INSERT INTO mur_groups (group_name, group_description, tab) VALUES ('".$name."', '".$desc."', '".$tab."')";
	} else if (isset($_POST['submit_page'])) {
		$name = $_POST['page_name'];
		$section = $_POST['add_to_section'];
		$url =  $_POST['page_link'];
		$desc =  $_POST['page_desc'];
		$query = "INSERT INTO mur_webpages (group_id, Title, link, description) VALUES ('".$section."','".$name."','".$url."','".$desc."')";
	}
	//echo $_POST['section_name']);
	//echo $query;
	if (mysqli_query($link, $query)) {
		$message = "Add was successful!";
	} else {
		$message = "Error! The request could not be completed. Please try again or see Brad.";
	}
}
//---------------------END OF PROCESSING----------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>MUR Admin - Add a Section</title>
    <link rel="stylesheet" href="../css/style.css" type="text/css" />
	<script src="http://code.jquery.com/jquery-latest.js"></script>
    <style>
	
	</style>
</head>
<body>
<div id="header">
	<div class="fullwrapper">
		<h1>Marketing & Undergraduate Recruiting - Page Management</h1>		
		<div id="login-name">
			<? echo '<h5>'.$_SESSION["authenticatedUserName"].'</h5>'; //We will use the same session system ?>
			<a href="">Logout</a>
			<div class="cleaner"></div>
		</div>
		<div class="cleaner"></div>
	</div>
</div>
<div id="content-frame" class="fullwrapper">
	<div class="section">
		<h1>Add A Section</h1>
		<form method="post" action="<? echo $_SERVER['PHP_SELF'];?>">
			<div style="width:980px;  float:left;"><div style="width:150px; float:left;">Section Name:</div><div style="width:350px; float:left;"><input type="text" name="section_name" style="width:275px;" /></div></div>
			<div style="width:980px;  float:left;"><div style="width:150px; float:left;">Description:</div><div style="width:350px; float:left;"><input type="text" name="section_desc" style="width:275px;" /></div></div>
			<div style="width:980px;  float:left;"><div style="width:150px; float:left;">Tab:</div><div style="width:350px; float:left;"><select name="add_to_tab" style="width:275px;">
			<option value="Systems">Systems</option>
			<option value="Reports">Reports</option>
			</select></div></div><br /><br />
			<input type="submit" name="submit_section" value="Add Section">
		</form>
	</div>
	<div class="section">
		<h1>Add A Page</h1>
		<form method="post" action="<?echo $_SERVER['PHP_SELF'];?>">
			<div style="width:980px;  float:left;"><div style="width:150px; float:left;">Page Name:</div><div style="width:350px; float:left;"><input type="text" name="page_name"style="width:275px;" /></div></div>
			<div style="width:980px;  float:left;"><div style="width:150px; float:left;">Page Link:</div><div style="width:350px; float:left;"><input type="text" name="page_link" style="width:275px;" /></div></div>
			<div style="width:980px;  float:left;"><div style="width:150px; float:left;">Description:</div><div style="width:350px; float:left;"><input type="text" name="page_desc" style="width:275px;" /></div></div>
			<div style="width:980px;  float:left;"><div style="width:150px; float:left;">Section:</div><div style="width:350px; float:left;"><select name="add_to_section" style="width:275px;">
			<?
				$query = "SELECT group_id, group_name FROM mur_groups ORDER BY group_name ASC";
				$result = mysqli_query($link, $query);
				while ($row = mysqli_fetch_array($result)) {
					echo '<option value="'.$row['group_id'].'">'.$row['group_name'].'</option>';
				}
			?>
			</select></div></div><br /><br />
			<input type="submit" name="submit_page" value="Add Page">
		</form>
	</div>
	<? 
	if (isset($message)) { //This message should be nicely styled.
		echo '<h2>'.$message.'</h2>';
	}
	?>
</div>
</html>