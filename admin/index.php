<?
require_once "../admin/login.php";
verifyPermissions(1);

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>User Management</title>
<link rel="stylesheet" href="css/global.css" type="text/css">
</head>



<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript">
	
	
	// Reset Div Functions
	function eraseDivs() {
		$("#contentDisplay").empty();
		$("#results").empty();	
		$("#resultsTop").empty();	
	}
	function eraseDivs_EntryDeleted() {
		$("#contentDisplay").empty();
		$("#results").empty();	
	}
	
	////// DISPLAY PAGE FUNCTIONS //////
	// on load:
	<?
	
	if (isset($_GET['page']) && isset($_GET['delete']))
	{
		if ($_GET['page'] == "addusers")
			echo "displayAddUsers(true);";
		else if ($_GET['page'] == "privileges")
			echo "displayPrivileges(true);";
	} 
	else
	{ 
		switch($_GET['page'])
		{
			case "addusers":
				echo "displayAddUsers();";
				break;
			case "addgroups":
				echo "displayAddGroups();";
				break;
			case "privileges":
				echo "displayPrivileges();";
				break;
			case "inactiveusers":
				echo "displayInactiveUsers();";
				break;
		}
	}
	?>
	
	$(document).ready(function() {
			$.post( 'ajax/displayAddUsers.php',
			function( data ) {
				$("#contentDisplay").html(data);
				window.history.replaceState(data, "Add Users", "index.php?page=addusers");
				$("#loading").attr('style', "float:right; display:none");

			});

	});
	
	
	function displayAddUsers(entryDeleted) {
		$("#loading").attr('style', "float:right; display:inline");
        $.post( 'ajax/displayAddUsers.php',
          function( data ) {
				if (!entryDeleted)
					eraseDivs();
				else
					eraseDivs_EntryDeleted();
				window.history.pushState(data, "Add Users", "index.php?page=addusers");
				$("#contentDisplay").html(data);
				$("#loading").attr('style', "float:right; display:none");
					
	      });
        
	} 
	function displayAddGroups(entryDeleted) {
		$("#loading").attr('style', "float:right; display:inline");

        $.post( 'ajax/displayAddGroups.php',
          function( data ) {
				if (!entryDeleted)
					eraseDivs();
				else
					eraseDivs_EntryDeleted();
				window.history.pushState(data, "Add Groups", "index.php?page=addgroups");	  
				$("#contentDisplay").html(data);
								$("#loading").attr('style', "float:right; display:none");
	
	      });
        
	} 
	function displayPrivileges(entryDeleted) {
		$("#loading").attr('style', "float:right; display:inline");

        $.post( 'ajax/displayPrivileges.php',
          function( data ) {
				if (!entryDeleted)
					eraseDivs();
				else
					eraseDivs_EntryDeleted();
				window.history.pushState(data, "Modify Privileges", "index.php?page=privileges");	
				$("#contentDisplay").html(data);	
								$("#loading").attr('style', "float:right; display:none");

	      });
        
	} 
	function displayInactiveUsers() {
		$("#loading").attr('style', "float:right; display:inline");

        $.post( 'ajax/displayInactiveUsers.php',
          function( data ) {
				eraseDivs();
				window.history.pushState(data, "Inactive Users", "index.php?page=inactiveusers");	
				$("#contentDisplay").html(data);	
								$("#loading").attr('style', "float:right; display:none");

	      });
        
	} 
	/**
	 * Fetches the content of the specified page using ajax, 
	 * and displays it in a div
	 * @author  Colin Armstrong <colin.armstrong@uwaterloo.ca>
	 * @return	formatted phone number if valid, false otherwise
	*/
	
	///////// FETCH RESULTS FUNCTIONS /////////
	function fetchInactiveUsers() {
        $.post( 'ajax/fetchInactiveUsers.php', $("#form1").serialize(),
          function( data ) {
				$("#results").html(data);	
	      });
        
	} 
	function fetchInactiveUsers_never() {
        $.post( 'ajax/fetchInactiveUsers.php', $("#form2").serialize(),
          function( data ) {
				$("#results").html(data);	
	      });
        
	} 
	function addUser() {
        $.post( 'ajax/addUser.php', $("#form1").serialize(),
          function( data ) {
			  	displayAddUsers(true);
				$("#resultsTop").html(data);	
	      });
        
	} 
	function addGroup() {
        $.post( 'ajax/addGroup.php', $("#form1").serialize(),
          function( data ) {
				displayAddGroups(true);
				$("#resultsTop").html(data);	
	      });
        
	} 
	function modifyPrivileges() {
        $.post( 'ajax/modifyPrivileges.php', $("#form1").serialize(),
          function( data ) {
			  	displayPrivileges(true);
				$("#resultsTop").html(data);	
	      });
      
	} 
	
	
	// Revert to a previously saved state
	window.addEventListener('popstate', function(event) {	
	  $('#contentDisplay').html(event.state);
	});
	
	
    </script>
<?php
if (isset($_GET['delete']) && isset($_GET['id']))
{
	if ($_GET['delete'])
	{
		
		if ($stmt = $link->prepare("DELETE FROM mur_users WHERE user_id = ? LIMIT 1"))
		{ 
			$user_id = escape_data($_GET['id']);
			$stmt->bind_param("i", $user_id);
			$stmt->execute();
			//$stmt->store_result();
		
			$numRows = $stmt->affected_rows;
			
			if ($numRows == 1)
				echo 'User deleted succesfully!';
		}
		
		?> 
	
		<script>
		
		$(document).ready(function(){
		
			$("#resultsTop").html("User deleted successfully!");
	
		});        
        
        </script> <?
	}
}

if (isset($_GET['delete']) && isset($_GET['uid']))
{
	if ($_GET['delete'])
	{		
		if ($stmt = $link->prepare("DELETE FROM mur_permission_listing WHERE uid = ? LIMIT 1"))
		{ 
			$user_id = $_GET['uid'];
			$stmt->bind_param("i", $user_id);
			if(!$stmt->execute())
				die("ERROR: $stmt->error");
		?> 
		<script>
		$(document).ready(function(){
			$("#resultsTop").html("User permission removed successfully!");
		});        
        
        </script> 
        <?    	
		} 
	}
}



?>

<body>
    <div id="wrapper">
        <div id="container">
            <div id="topwrap"><a href="<?php echo "index.php" ?>" class="button fleft">Home</a><a href="?logout=true" class="button fright">Log out</a>
                <h1 id="header">User Management </h1>
                <div id="header" style="font-size:12px">
                    <div class="center">
                        <a onClick="displayAddUsers()" class="button spark">Add Users</a>
                        <a onClick="displayAddGroups();" class="button spark">Add Groups</a>   
                        <a onClick="displayPrivileges();" class="button spark">Modify Privileges</a>           
                        <a onClick="displayInactiveUsers();" class="button spark">Inactive Users</a>    
                    </div>
                </div>
            </div>
            <div id="updates"></div>
            <div id="content">
            	<div id="resultsTop"></div>
				<div id="contentDisplay"></div>
                <div id="results"></div>
            </div>
        </div>
    </div>
</body>
</html>