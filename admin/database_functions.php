<?php 

/*The file was created to host various database related functions that the various applications make use of
*/
// Function for escaping and trimming form data.
function escape_data ($data) { 
	global $dbc;
	if (ini_get('magic_quotes_gpc')) {
		$data = stripslashes($data);
	}
	return mysql_real_escape_string (trim ($data), $dbc);
} // End of escape_data() function.
?>

?>