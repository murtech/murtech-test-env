<?php

function grouppermission($a = NULL) { //This method is used to prevent having to remove the grouppermission call from every file. This is a legacy hack.
	return true; 
}

// Main ----------

session_start();

if (!isset($_SESSION['authenticatedUserId'])) {
	//code to obtain URL
	function curPageURL() 
	{
 	$pageURL = 'http';
 		if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 		$pageURL .= "://";
 		if ($_SERVER["SERVER_PORT"] != "80") {
  		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER[	 	"REQUEST_URI"];
 		}else {
  		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 		}
 			return $pageURL;
	}
			
	$_SESSION['pageURL']= curPageURL(); //store the intended URL for redirection
	header("Location: http://pole.uwaterloo.ca/urp/urpdev/");
	exit;
}
      
?>

